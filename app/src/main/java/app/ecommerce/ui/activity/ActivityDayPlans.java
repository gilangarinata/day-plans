package app.ecommerce.ui.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.Toast;

import java.util.Collections;
import java.util.List;

import app.ecommerce.ui.R;
import app.ecommerce.ui.adapter.AdapterDayPlans;
import app.ecommerce.ui.data.DataGenerator;
import app.ecommerce.ui.model.DayPlans;
import app.ecommerce.ui.utils.Tools;
import app.ecommerce.ui.utils.ViewAnimation;

/**
 * Created by Gilang Prilian Arinata on 07/02/20.
 * Email : gilangarinata@gmail.com
 */

public class ActivityDayPlans extends AppCompatActivity {
    private NestedScrollView scroll_view;
    private ImageButton btn_arrow;
    private View layout_main;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_day_plans);

        initToolbar();
        initComponent();
    }

    private void initToolbar() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Day plans");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        Tools.setSystemBarColor(this);
    }

    private void initComponent() {
        btn_arrow =  findViewById(R.id.btn_arrow);
        layout_main = findViewById(R.id.lyt_content_main);
        scroll_view = findViewById(R.id.nested_scroll_view);

        btn_arrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                animateExpand(btn_arrow);
            }
        });

        //set first recyclerview item
        RecyclerView recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL,false));

        List<DayPlans> items = DataGenerator.getImageDayPlansOne(this);

        AdapterDayPlans mAdapter = new AdapterDayPlans(this, items);
        recyclerView.setAdapter(mAdapter);

        //set 2nd recyclerview item
        RecyclerView recyclerView2 = findViewById(R.id.recyclerView2);
        recyclerView2.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL,false));

        List<DayPlans> items2 = DataGenerator.getImageDayPlansTwo(this);

        AdapterDayPlans mAdapter2 = new AdapterDayPlans(this, items2);
        recyclerView2.setAdapter(mAdapter2);
    }

    private void animateExpand(View view) {
        boolean show = stateArrow(view);
        if (!show) {
            ViewAnimation.expand(layout_main, new ViewAnimation.AnimListener() {
                @Override
                public void onFinish() {
                    Tools.nestedScrollTo(scroll_view, layout_main);
                }
            });
        } else {
            ViewAnimation.collapse(layout_main);
        }
    }

    public boolean stateArrow(View view) {
        if (view.getRotation() == 0) {
            view.animate().setDuration(200).rotation(180);
            return true;
        } else {
            view.animate().setDuration(200).rotation(0);
            return false;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.example_menu, menu);
        Tools.changeMenuIconColor(menu, getResources().getColor(R.color.grey_60));
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        } else {
            Toast.makeText(getApplicationContext(), item.getTitle(), Toast.LENGTH_SHORT).show();
        }
        return super.onOptionsItemSelected(item);
    }
}

package app.ecommerce.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.recyclerview.widget.RecyclerView;


import java.util.List;

import app.ecommerce.ui.R;
import app.ecommerce.ui.model.DayPlans;
import app.ecommerce.ui.utils.Tools;

public class AdapterDayPlans extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private List<DayPlans> items;
    private Context context;

    public AdapterDayPlans(Context context, List<DayPlans> items) {
        this.items = items;
        this.context = context;
    }

    public class OriginalViewHolder extends RecyclerView.ViewHolder {
        public ImageView image;

        public OriginalViewHolder(View v) {
            super(v);
            image = v.findViewById(R.id.image);
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder vh;
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_day_plan, parent, false);
        vh = new OriginalViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        DayPlans obj = items.get(position);
        if (holder instanceof OriginalViewHolder) {
            OriginalViewHolder view = (OriginalViewHolder) holder;
            Tools.displayImageOriginal(context, view.image, obj.image);
        }
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

}